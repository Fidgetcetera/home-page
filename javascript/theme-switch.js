toggleTheme = function() {
  if (document.getElementById('color-theme').getAttribute("href") === '/css/colors/light.css') {
    setTheme("dark", "light")
    if (localStorage.getItem("theme")) {
      localStorage.removeItem("theme");
    }
    localStorage.setItem("theme", "dark");
  } else {
    setTheme("light", "dark")
    if (localStorage.getItem("theme")) {
      localStorage.removeItem("theme");
    }
    localStorage.setItem("theme", "light");
  }
};


setTheme = function(newTheme, oldTheme) {
  document.getElementById('color-theme').href = '/css/colors/' + newTheme + '.css';
  document.getElementById('theme-switcher').innerHTML = "Switch to " + oldTheme + " theme?";
};

if (localStorage.getItem("theme") && localStorage.getItem("theme") === 'light') {
  setTheme("light", "dark");
} else if (localStorage.getItem("theme") && localStorage.getItem("theme") === 'dark') {
  setTheme("dark", "light");
} else if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
  setTheme("dark", "light");
} else if (window.matchMedia('(prefers-color-scheme: light)').matches) {
  setTheme("light", "dark");
};
