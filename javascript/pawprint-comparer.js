textIsEqualWithoutSpaces = function(firstText, secondText) {
  return (firstText.replace(/\s/g,'') === secondText.replace(/\s/g,''))
}

testKeyEqual = function(idOfPawprint, idOfTextBox, idOfTextIndicator, isCaseSensitive){
  let textBoxElement = document.getElementById(idOfTextBox);
  let pastedText = textBoxElement.value;
  let pawprintTextElement = document.getElementById(idOfPawprint)
  let pawprintText = pawprintTextElement.innerHTML;
  let textIndicator = document.getElementById(idOfTextIndicator);
  if (isCaseSensitive === false){
    pastedText = pastedText.toLowerCase();
    pawprintText = pawprintText.toLowerCase();
  }
  if (textIsEqualWithoutSpaces(pastedText, pawprintText)){
    textIndicator.classList = "";
    textIndicator.innerText = "✔️ These match!";
  }
  else{
    textIndicator.classList = "";
    textIndicator.innerText = "❌ These don't match!";
  }
}
